package it.unibas.unibaslog;

import it.unibas.unibaslog.modello.encoder.IEncoderStrategy;
import it.unibas.unibaslog.modello.encoder.PlainerStrategy;
import it.unibas.unibaslog.modello.out.IOutputterStrategy;
import it.unibas.unibaslog.modello.out.TextStrategy;
import it.unibas.unibaslog.modello.out.decorator.XmlDecorator;
import it.unibas.unibaslog.modello.out.decorator.TextVerbose;
import it.unibas.unibaslog.modello.out.decorator.XmlVerbose;
import it.unibas.unibaslog.modello.recorder.RecorderChain;
import it.unibas.unibaslog.modello.sender.MessaggioLog;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Vincenzo Palazzo
 */
public class LoggerFacede {

    private IOutputterStrategy outputter;
    private RecorderChain recorder;
    private IEncoderStrategy encoder;

    public LoggerFacede() {
        this(new TextStrategy(), new RecorderChain(), new PlainerStrategy());
    }

    public LoggerFacede(IOutputterStrategy outputter, RecorderChain recorder, IEncoderStrategy encoder) {
        this.outputter = outputter;
        this.recorder = recorder;
        this.encoder = encoder;
    }

    public LoggerFacede(IOutputterStrategy outputter) {
         this(outputter, new RecorderChain(), new PlainerStrategy());
    }

    public LoggerFacede(RecorderChain recorder) {
         this(new TextStrategy(), recorder, new PlainerStrategy());
    }
    
    public LoggerFacede(IEncoderStrategy encoder) {
         this(new TextStrategy(), new RecorderChain(), encoder);
    }
    
    public void enableVerbose(){
        if(outputter instanceof XmlDecorator){
            outputter = new XmlVerbose(outputter);
            return;
        }
        outputter = new TextVerbose(outputter);
    }

    public void setOutputter(IOutputterStrategy outputter) {
        boolean flag = false;
        if((this.outputter instanceof XmlVerbose) == true || (this.outputter instanceof TextVerbose) == true){
            flag = true;
        }
        this.outputter = outputter;
        if(flag){
            enableVerbose();
        }
    }

    public void setRecorder(RecorderChain recorder) {
        
        
        this.recorder = recorder;
    }

    public void setEncoder(IEncoderStrategy encoder) {
        this.encoder = encoder;
    }

    public RecorderChain getRecorder() {
        return recorder;
    }

    public IEncoderStrategy getEncoder() {
        return encoder;
    }
    
    
    
    public void disableVerbose(){
        if(outputter instanceof XmlVerbose){
            outputter = new XmlDecorator(new TextStrategy());
            return;
        }
        outputter = new TextStrategy();
    }
    

    public void debug(String messaggio) {
        log(messaggio, Constanti.LIVELLO_DEBUG);
    }

    public void info(String messaggio) {
        log(messaggio, Constanti.LIVELLO_INFO);
    }

    public void error(String messaggio) {
        log(messaggio, Constanti.LIVELLO_ERROR);
    }

    public void warning(String messaggio) {
        log(messaggio, Constanti.LIVELLO_WARNING);
    }

    private void log(String messaggio, String livello) {
        //TODO gestire questo metodo in base alla classe LogMessage
        
        String encodeString = encoder.encode(messaggio);
        MessaggioLog output = outputter.print(encodeString, livello);
        recorder.run(output.toString());
    }

}
