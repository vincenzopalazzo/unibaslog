/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.persistenza;

import java.util.List;

/**
 *
 * @author Vincenzo Palazzo
 */
public interface IDAORecorder {
    
    void save(String messaggioLogger);
    
    List<String> getMessage();
    
}
