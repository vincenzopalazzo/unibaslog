/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.persistenza;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vincenzo Palazzo
 */
public class DAODatabase implements IDAORecorder{
    
    private List<String> log = new ArrayList<>();

    @Override
    public void save(String messaggioLogger) {
        log.add(messaggioLogger);
    }

    @Override
    public List<String> getMessage() {
        return log;
    }
    
}
