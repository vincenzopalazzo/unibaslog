/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.persistenza;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Vincenzo Palazzo
 */
public class DAORecorderFactory {
    
    private static DAORecorderFactory SINGLETON;
    
    private Map<String, IDAORecorder> cache = new HashMap<>();
    
    private DAORecorderFactory(){}
    
    public static DAORecorderFactory getIstance(){
        if(SINGLETON == null){
            SINGLETON = new DAORecorderFactory();
        }
        return SINGLETON;
    }
    
    public IDAORecorder getRecorder(String key){
        return cache.get(key);
    }
    
    public void putRecorder(String key, IDAORecorder dao){
        cache.put(key, dao);
    }
}
