/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog;

import it.unibas.unibaslog.modello.out.TextStrategy;
import it.unibas.unibaslog.modello.out.decorator.XmlDecorator;

/**
 *
 * @author Vincenzo Palazzo
 */
public class App {
    
    private static final LoggerFacede LOGGER = new LoggerFacede();
    
    public static void main(String[] args) {
        LOGGER.debug("Siamo all'interno del main e stiamo testando il nuovo logger");
        
        LOGGER.warning("Sto comabiando il modo di generare il messaggio, il prossimo sara xml verbose");
        LOGGER.setOutputter(new XmlDecorator(new TextStrategy()));
        LOGGER.enableVerbose();
        LOGGER.debug("Questa stampa di log e' in xml e e' anche verbose");
        LOGGER.setOutputter(new TextStrategy());
        LOGGER.error("quando cambio outputter con il verbose abilitato vorrei che il vorbose rimanesse");
    }
    
}
