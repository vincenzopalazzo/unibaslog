/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog;

/**
 *
 * @author Vincenzo Palazzo
 */
public class Constanti {

    public static final String LIVELLO_INFO = "INFO";
    public static final String LIVELLO_DEBUG = "DEBUG";
    public  static final String LIVELLO_ERROR = "ERROR";
    public  static final String LIVELLO_WARNING = "WARNING";
    
}
