/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.sender;

import it.unibas.unibaslog.Constanti;
import java.util.GregorianCalendar;

/**
 *
 * @author Vincenzo Palazzo
 */
public class MessaggioLog {
    
    private String message;
    private GregorianCalendar calendar;
    private String level;
    private String messageLog;

    public MessaggioLog(String message, GregorianCalendar calendar, String level) {
        this.message = message;
        this.calendar = calendar;
        this.level = level;
    }

    public MessaggioLog(String message, String level) {
        this(message, new GregorianCalendar(), level);
    }

    public String getMessage() {
        return message;
    }

    public String getMessageLog() {
        return messageLog;
    }

    public void setMessageLog(String messageLog) {
        this.messageLog = messageLog;
    }

    @Override
    public String toString() {
        return  messageLog;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof String){
            return messageLog.equals(obj);
        }
        MessaggioLog messageLogObj = (MessaggioLog) obj;
        return this.messageLog.equals(messageLogObj.getMessageLog());
    }
    
    

    public GregorianCalendar getCalendar() {
        return calendar;
    }

    public String getLevel() {
        return level;
    }
    
}
