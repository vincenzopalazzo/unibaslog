/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.encoder;

/**
 *
 * @author Vincenzo Palazzo
 */
public interface IEncoderStrategy {
    
    String encode(String messaggio);
    
}
