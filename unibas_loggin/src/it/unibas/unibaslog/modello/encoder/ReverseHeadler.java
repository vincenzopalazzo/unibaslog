/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.encoder;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ReverseHeadler implements ICrypterHeadler{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ReverseHeadler.class);
    
    private int peso;

    public ReverseHeadler(int peso) {
        this.peso = peso;
    }

    public ReverseHeadler() {
    }

    
    
    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }
    
    @Override
    public String encode(String messaggio) {
        StringBuilder stringReverse = new StringBuilder(messaggio);
        String reverse = stringReverse.reverse().toString();
        LOGGER.debug("Message NON reverse: " + messaggio);
        LOGGER.debug("Message reverse: " + reverse);
        return reverse;
    }

    @Override
    public int compareTo(ICrypterHeadler o) {
        return ((Integer) peso).compareTo(o.getPeso());
    }
}
