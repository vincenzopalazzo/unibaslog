/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.encoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class CrypterChainStrategy implements IEncoderStrategy{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CrypterChainStrategy.class);
    
    private List<ICrypterHeadler> headler = new ArrayList<>();
    
    public void appenEncoder(ICrypterHeadler encoder){
        headler.add(encoder);
    }

    @Override
    public String encode(String messaggio) {
        if(headler.isEmpty()){
            LOGGER.debug("Modalita crypter non impostata");
            throw new IllegalArgumentException("Modalita crypter non impostata");
        }
        Collections.sort(headler);
        String encodeString = messaggio;
        for(ICrypterHeadler crypterHeadler : headler){
            encodeString = crypterHeadler.encode(encodeString);
        }
        return encodeString;
    }
    
}
