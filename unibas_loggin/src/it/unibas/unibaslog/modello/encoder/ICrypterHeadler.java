/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.encoder;

/**
 *
 * @author Vincenzo Palazzo
 */
public interface ICrypterHeadler extends Comparable<ICrypterHeadler>{
 
    String encode(String messaggio);

    public int getPeso();
    public void setPeso(int peso);
}
