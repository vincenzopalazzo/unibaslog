/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.encoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ExaHeadler implements ICrypterHeadler{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ExaHeadler.class);
    
    private int peso;

    public ExaHeadler(int peso) {
        this.peso = peso;
    }

    public ExaHeadler() {}

    @Override
    public String encode(String messaggio) { //TODO implementato parzialmente
        messaggio = messaggio.replace(" ", "0");
        messaggio = messaggio.replace("i", "11");
        messaggio = messaggio.replace("a", "44");
        messaggio = messaggio.replace("I", "11");
        messaggio = messaggio.replace("A", "44");
        LOGGER.debug("Messaggio con cripcato con EXA: " + messaggio);
        return messaggio;
    }

    @Override
    public int getPeso() {
        return peso;
    }

    @Override
    public void setPeso(int peso) {
        this.peso = peso;
    }

    @Override
    public int compareTo(ICrypterHeadler o) {
        return ((Integer)peso).compareTo(o.getPeso());
    }
    
}
