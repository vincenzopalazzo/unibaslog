/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.recorder.headler;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ConsoleHeadler implements IRecorderHeadler{

    @Override
    public void print(String messaggio) {
        if(messaggio.contains("ERROR") || messaggio.contains("error")){
            System.err.println(messaggio);
            return;
        }
        System.out.println(messaggio);
    }
    
}
