/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.recorder;

import it.unibas.unibaslog.modello.recorder.headler.IRecorderHeadler;
import it.unibas.unibaslog.LoggerFacede;
import it.unibas.unibaslog.modello.recorder.headler.ConsoleHeadler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class RecorderChain {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RecorderChain.class);
    
    private List<IRecorderHeadler> headler = new ArrayList<>();

    public RecorderChain() {
        headler.add(new ConsoleHeadler());
    }
    
    public void run(String messaggio){
       for(IRecorderHeadler recorderHeadler : headler){
           recorderHeadler.print(messaggio);
       }
    }
        
    
}
