/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.out.decorator;

import com.sun.javafx.binding.StringFormatter;
import it.unibas.unibaslog.modello.out.IOutputterStrategy;
import it.unibas.unibaslog.modello.sender.MessaggioLog;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class XmlVerbose extends MessageDecorator{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlVerbose.class);
    
    public XmlVerbose(IOutputterStrategy outputter) {
        super(outputter);
    }

    @Override
    public MessaggioLog print(String messaggio, String livello) {
        if((outputter instanceof XmlDecorator) == false){
            LOGGER.error("Si e' verificato un errore nei tipi");
            throw new IllegalArgumentException("Tipo non corretto, mi aspetto un " 
                    + XmlDecorator.class.getCanonicalName());
        }
        LOGGER.debug("la classica stampa di xml è: " + super.print(messaggio, livello));
        MessaggioLog message = super.print(messaggio, livello);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DATE_FIELD);
        String dataString = dateFormat.format(message.getCalendar().getTime());
        LOGGER.debug("Data: " + dataString);
        SimpleDateFormat oreFormat = new SimpleDateFormat("k");
        String oraString = oreFormat.format(message.getCalendar().getTime());
        LOGGER.debug("Ora: " + oraString);
        SimpleDateFormat minutiFormat = new SimpleDateFormat("m");
        String minuteString = minutiFormat.format(message.getCalendar().getTime());
        LOGGER.debug("minuti: " + minuteString);
        String log = "<log>\n    <" + message.getLevel().toLowerCase() + ">" + message.getMessage().toLowerCase() 
                + "</" + message.getLevel().toLowerCase() + ">\n    <data>" + dataString + "</data>\n"
                + "    <time><hh>" + oraString + "</hh><mm>" + minuteString + "</mm>\n</log>";
        LOGGER.debug(log);
        message.setMessageLog(log);
        return message;
    }
    
    
    
}
