/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.out.decorator;

import it.unibas.unibaslog.modello.out.IOutputterStrategy;
import it.unibas.unibaslog.modello.out.IOutputterStrategy;
import it.unibas.unibaslog.modello.sender.MessaggioLog;

/**
 *
 * @author Vincenzo Palazzo
 */
public class MessageDecorator implements IOutputterStrategy{

    IOutputterStrategy outputter;

    public MessageDecorator(IOutputterStrategy outputter) {
        this.outputter = outputter;
    }
    
    
    @Override
    public MessaggioLog print(String messaggio, String livello) {
        return outputter.print(messaggio, livello);
    }
    
}
