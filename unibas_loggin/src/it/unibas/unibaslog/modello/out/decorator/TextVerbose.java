/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.out.decorator;

import it.unibas.unibaslog.modello.out.IOutputterStrategy;
import it.unibas.unibaslog.modello.sender.MessaggioLog;
import java.text.DateFormat;
import java.util.GregorianCalendar;

/**
 *
 * @author Vincenzo Palazzo
 */
public class TextVerbose extends MessageDecorator{
    
    public TextVerbose(IOutputterStrategy outputter) {
        super(outputter);
    }

    @Override
    public MessaggioLog print(String messaggio, String livello) {
        MessaggioLog message = super.print(messaggio, livello);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        String dateString = dateFormat.format(message.getCalendar().getTime());
        String logVerbose = dateString + " " + message.getLevel().toUpperCase() 
                + " - " + message.getMessage().toLowerCase();
        message.setMessageLog(logVerbose);
        return message;
    }
    
    
    
}
