/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.out.decorator;

import it.unibas.unibaslog.modello.out.IOutputterStrategy;
import it.unibas.unibaslog.modello.sender.MessaggioLog;

/**
 *
 * @author Vincenzo Palazzo
 */
public class XmlDecorator extends MessageDecorator{

    public XmlDecorator(IOutputterStrategy outputter) {
        super(outputter);
    }

    @Override
    public MessaggioLog print(String messaggio, String livello) {
        MessaggioLog message = super.print(messaggio, livello);
        String out = "<log> \n      <" + message.getLevel().toLowerCase() + ">" 
                + message.getMessage().toLowerCase() + "</" + message.getLevel().toLowerCase() + ">\n</log>";
        message.setMessageLog(out);
        return message;
    }
    
}
