/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.out;

import it.unibas.unibaslog.Constanti;
import it.unibas.unibaslog.modello.sender.MessaggioLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class TextStrategy implements IOutputterStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextStrategy.class);

    @Override
    public MessaggioLog print(String messaggio, String livello) {
        if (messaggio == null) {
            LOGGER.error("Errore nell'input");
            throw new IllegalArgumentException("Errore nell'input");
        }
        if (!isLevelValid(livello)) {
            LOGGER.error("Livello non riconosciuto, inpusto un livello ti tipo debug");
            livello = Constanti.LIVELLO_DEBUG;
        }
        MessaggioLog message = new MessaggioLog(messaggio, livello);
        message.setMessageLog(message.getLevel().toUpperCase() + " - " + message.getMessage().toLowerCase());
        return message;
    }

    private boolean isLevelValid(String livello) {
        if(livello == null){
            return false;
        }
        if (livello.equalsIgnoreCase(Constanti.LIVELLO_INFO)) {
            return true;
        } else if (livello.equalsIgnoreCase(Constanti.LIVELLO_ERROR)) {
            return true;
        } else if (livello.equals(Constanti.LIVELLO_WARNING)) {
            return true;
        } else if (livello.equalsIgnoreCase(Constanti.LIVELLO_DEBUG)) {
            return true;
        }
        return false;
    }

}
