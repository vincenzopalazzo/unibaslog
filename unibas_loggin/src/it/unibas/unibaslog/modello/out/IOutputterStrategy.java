/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.out;

import it.unibas.unibaslog.modello.sender.MessaggioLog;


/**
 *
 * @author Vincenzo Palazzo
 */
public interface IOutputterStrategy {
    
    MessaggioLog print(String messaggio, String livello);
    
}
