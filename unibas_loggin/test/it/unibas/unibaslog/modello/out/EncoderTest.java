/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unibas.unibaslog.modello.out;

import it.unibas.unibaslog.modello.out.decorator.XmlDecorator;
import it.unibas.unibaslog.Constanti;
import it.unibas.unibaslog.modello.encoder.CrypterChainStrategy;
import it.unibas.unibaslog.modello.encoder.ExaHeadler;
import it.unibas.unibaslog.modello.encoder.ReverseHeadler;
import it.unibas.unibaslog.modello.sender.MessaggioLog;
import junit.framework.TestCase;

/**
 *
 * @author Vincenzo Palazzo
 */
public class EncoderTest extends TestCase {

    CrypterChainStrategy crypter;

    @Override
    protected void setUp() throws Exception {
        crypter = new CrypterChainStrategy();
    }

    public void testReverseTexUno() {
        IOutputterStrategy outputterStrategy = new TextStrategy();
        crypter.appenEncoder(new ReverseHeadler());
        String cripterString = crypter.encode("Questo e' un test dell'algoritmo revers");
        MessaggioLog meString = outputterStrategy.print(cripterString, Constanti.LIVELLO_DEBUG);
        
        String equivalLog = "DEBUG - srever omtirogla'lled tset nu 'e otseuq";
        assertEquals(equivalLog, meString.toString());
    }

    public void testReverseXmlUno() {
        crypter.appenEncoder(new ReverseHeadler());
        String endProcess = crypter.encode("Questo e' un test dell'algoritmo revers");
        IOutputterStrategy outputterStrategy = new XmlDecorator(new TextStrategy());
        MessaggioLog meString = outputterStrategy.print(endProcess, Constanti.LIVELLO_DEBUG);
        assertTrue(meString.toString().contains("srever omtirogla'lled tset nu 'e otseu"));
    }

    public void testExaTextUno() {
        crypter.appenEncoder(new ExaHeadler());
        String endProcess = crypter.encode("Questo e' un test dell'algoritmo revers");
        IOutputterStrategy outputterStrategy = new TextStrategy();
        MessaggioLog meString = outputterStrategy.print(endProcess, Constanti.LIVELLO_DEBUG);
        String equivalLog = "DEBUG - questo0e'0un0test0dell'44lgor11tmo0revers";
        assertEquals(equivalLog, meString.toString());
    }

    public void testExaXmlUno() {
        crypter.appenEncoder(new ExaHeadler());
        String endProcess = crypter.encode("Questo e' un test dell'algoritmo revers");
        IOutputterStrategy outputterStrategy = new XmlDecorator(new TextStrategy());
        MessaggioLog meString = outputterStrategy.print(endProcess, Constanti.LIVELLO_DEBUG);
        assertTrue(meString.toString().contains("questo0e'0un0test0dell'44lgor11tmo0revers"));
    }

    public void textDoubleCrypterText() {
        crypter.appenEncoder(new ExaHeadler(1));
        crypter.appenEncoder(new ReverseHeadler(0));
        String endProcess = crypter.encode("Questo e' un test dell'algoritmo revers");
        IOutputterStrategy outputterStrategy = new TextStrategy();
        MessaggioLog meString = outputterStrategy.print(endProcess, Constanti.LIVELLO_INFO);
        String equivalLog = "srever0omt11rogl44'lled0tset0nu0'e0otseuq - GUBED";
        assertEquals(equivalLog, meString.toString());
    }

    public void textDoubleCrypterXml() {
        crypter.appenEncoder(new ExaHeadler(1));
        crypter.appenEncoder(new ReverseHeadler(0));
        String endProcess = crypter.encode("Questo e' un test dell'algoritmo revers");
        IOutputterStrategy outputterStrategy = new XmlDecorator(new TextStrategy());
        MessaggioLog meString = outputterStrategy.print(endProcess, Constanti.LIVELLO_INFO);
        assertTrue(meString.toString().contains("srever0omt11rogl44'lled0tset0nu0'e0otseuq"));
    }

}
