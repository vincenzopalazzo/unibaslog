package it.unibas.unibaslog.modello.out;

import it.unibas.unibaslog.modello.out.decorator.XmlDecorator;
import it.unibas.unibaslog.modello.out.decorator.MessageDecorator;
import it.unibas.unibaslog.modello.out.decorator.TextVerbose;
import junit.framework.TestCase;
import it.unibas.unibaslog.Constanti;
import it.unibas.unibaslog.modello.out.decorator.XmlVerbose;
import it.unibas.unibaslog.modello.sender.MessaggioLog;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author Vincenzo Palazzo
 */
public class OutputterTest extends TestCase {

    private static final Logger LOGGER = LoggerFactory.getLogger(OutputterTest.class);

    IOutputterStrategy texStrategy;
    IOutputterStrategy xmlStrategy;

    @Override
    protected void setUp() throws Exception {
        texStrategy = new TextStrategy();
        xmlStrategy = new XmlDecorator(texStrategy);
    }

    public void testTextNoVerboseUno() {
        MessaggioLog log = texStrategy.print("Questa è una stampa di log", Constanti.LIVELLO_INFO);
        String logAtteso = "INFO - questa è una stampa di log";
        assertEquals(logAtteso, log.toString());
    }

    public void testTextNoVerboseNullText() {
        try {
            MessaggioLog log = texStrategy.print(null, Constanti.LIVELLO_INFO);
            fail("Non si e' vetrifitata IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
            assertEquals("Test superato", "Test superato");
        }
    }

    public void testTextNoVerboseNotLevelCorrect() {
        MessaggioLog log = texStrategy.print("Non inseriro' un livello corretto", "Pippi calze lunghe");
        String logAtteso = "DEBUG - non inseriro' un livello corretto";
        assertEquals(logAtteso, log.toString());
    }
    
    public void testTextNoVerboseLevelNull() {
        MessaggioLog log = texStrategy.print("Inseriro' un livello nullo", null);
        String logAtteso = "DEBUG - inseriro' un livello nullo";
        assertEquals(logAtteso, log.toString());
    }

    public void testTextNoVerboseDue() {
        MessaggioLog log = texStrategy.print("Questa e' la seconda stampa Di Log", Constanti.LIVELLO_DEBUG);
        String logAtteso = "DEBUG - questa e' la seconda stampa di log";
        assertEquals(logAtteso, log.toString());
    }

    public void testTexVerbose() {
        MessageDecorator verboseDecorator = new TextVerbose(texStrategy);
        MessaggioLog verLog = verboseDecorator.print("Questa è una stampa di log", Constanti.LIVELLO_INFO);
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DATE_FIELD);
        String dataString = dateFormat.format(gregorianCalendar.getTime());
        String logAtteso = dataString + " INFO - questa è una stampa di log";
        assertEquals(logAtteso, verLog.toString());
    }

    public void testXmlNoVerboseTagLiverllo() {
        MessaggioLog text = xmlStrategy.print("Questa e' una stampa di log", Constanti.LIVELLO_INFO);
        assertTrue(text.toString().contains("<info>"));
        assertTrue(text.toString().contains("</info>"));

    }

    public void testXmlNoVerboseTagLog() {
        MessaggioLog text = xmlStrategy.print("Questa e' una stampa di log", Constanti.LIVELLO_INFO);
        assertTrue(text.toString().contains("<log>"));
        assertTrue(text.toString().contains("</log>"));
    }

    public void testXmlNoVerboseTagMessaggio() {
        MessaggioLog text = xmlStrategy.print("Questa e' una stampa di log", Constanti.LIVELLO_INFO);
        assertTrue(text.toString().contains("questa e' una stampa di log"));
    }

    public void testXmlVerboseLog() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = xmlStrategy.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        assertTrue(logVerbose.toString().contains("<log>"));
        assertTrue(logVerbose.toString().contains("</log>"));
    }

    public void testXmlVerboseDate() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = verboseXml.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        assertTrue(logVerbose.toString().contains("<data>"));
        assertTrue(logVerbose.toString().contains("</data>"));
    }

    public void testXmlVerboseLivello() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = verboseXml.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        assertTrue(logVerbose.toString().contains("<debug>"));
        assertTrue(logVerbose.toString().contains("</debug>"));
    }

    public void testXmlVerboseMinuti() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = verboseXml.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        assertTrue(logVerbose.toString().contains("<mm>"));
        assertTrue(logVerbose.toString().contains("</mm>"));
    }

    public void testXmlVerboseOre() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = verboseXml.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        assertTrue(logVerbose.toString().contains("<hh>"));
        assertTrue(logVerbose.toString().contains("</hh>"));
    }

    public void testXmlVerboseValueOre() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = verboseXml.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        SimpleDateFormat formatiOre = new SimpleDateFormat("k");
        assertTrue(logVerbose.toString().contains(formatiOre.format(gregorianCalendar.getTime())));
    }

    public void testXmlVerboseValueMinuti() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = verboseXml.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        SimpleDateFormat formatiOre = new SimpleDateFormat("m");
        assertTrue(logVerbose.toString().contains(formatiOre.format(gregorianCalendar.getTime())));
    }

    public void testXmlVerboseValueData() {
        IOutputterStrategy verboseXml = new XmlVerbose(xmlStrategy);
        MessaggioLog logVerbose = verboseXml.print("Questa e' una stampa di log", Constanti.LIVELLO_DEBUG);
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DATE_FIELD);
        String date = dateFormat.format(gregorianCalendar.getTime());
        assertTrue(logVerbose.toString().contains(date));
    }

}
